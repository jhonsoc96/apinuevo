<?php $__env->startSection('content'); ?>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header card-header-primary">
                    <h4 class="card-title">Usuarios</h4>
                    <p class="card-category">Usuarios registrados</p>
                  </div>
                  <div class="card-body">
                    <?php if(session('success')): ?>
                    <div class="alert alert-success" role="success">
                      <?php echo e(session('success')); ?>

                    </div>
                    <?php endif; ?>
                    <div class="row">
                      <div class="col-12 text-right">

                        <a href="<?php echo e(route('users.create')); ?>" class="btn btn-sm btn-facebook">Añadir usuario</a>

                      </div>
                    </div>
                    <div class="table-responsive">
                      <table class="table">
                        <thead class="text-primary">
                          <th>ID</th>
                          <th>Nombre</th>
                          <th>Correo</th>
                          <th>Username</th>
                          <th>created_at</th>
                          <th class="text-right">Acciones</th>
                        </thead>
                        <tbody>
                          <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                              <td><?php echo e($user->id); ?></td>
                              <td><?php echo e($user->name); ?></td>
                              <td><?php echo e($user->email); ?></td>
                              <td><?php echo e($user->username); ?></td>
                              <td><?php echo e($user->created_at); ?></td>

                              <td class="td-actions text-right">
                               <!--  <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user_show')): ?>
                                <a href="<?php echo e(route('users.show', $user->id)); ?>" class="btn btn-info"><i class="material-icons">person</i></a>
                                <?php endif; ?> -->

                                <a href="<?php echo e(route('users.edit', $user->id)); ?>" class="btn btn-warning"><i class="material-icons">edit</i></a>


                                <form action="<?php echo e(route('users.delete', $user->id)); ?>" method="POST" style="display: inline-block;" onsubmit="return confirm('Seguro?')">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('DELETE'); ?>
                                    <button class="btn btn-danger" type="submit" rel="tooltip">
                                    <i class="material-icons">close</i>
                                    </button>
                                </form>

                              </td>
                            </tr>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer mr-auto">
                    <?php echo e($users->links()); ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', ['activePage' => 'users', 'titlePage' => 'Usuarios'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\APInuevo\resources\views/users/index.blade.php ENDPATH**/ ?>