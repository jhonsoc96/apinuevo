<?php

namespace App\Http\Controllers;

use App\Models\User;
use GrahamCampbell\ResultType\Success;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(){
        $users = User::paginate(5);
        return view('users.index',compact('users'));
    }

    public function create(){
        return view('users.create');
    }

    public function store(Request $request){
        User::create($request->all());
        return redirect()->route('users.index');
    }

    public function edit(User $user)
    {

        return view('users.edit', compact('user'));
    }

    public function update(Request $request,$id){
        $user=User::findOrFail($id);
        $data = $request->only('name', 'email');
        $password=$request->input('password');
        if($password)
            $data['password'] = bcrypt($password);
         if(trim($request->password)=='')
         {
             $data=$request->except('password');
         }
         else{
             $data=$request->all();
            $data['password']=bcrypt($request->password);
        }

        $user->update($data);

        /* $roles = $request->input('roles', []);
        $user->syncRoles($roles); */
        return redirect()->route('users.index')->with('success','Usuario Actualizado Correctamente');
    }

    public function destroy(User $user)
    {

        $user->delete();
        return back()->with('succes', 'Usuario eliminado correctamente');
    }
}
